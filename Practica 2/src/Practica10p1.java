
import java.util.Scanner;

/**
 *
 * @author BAAM
 */
public class Practica10p1 {

    public static void main(String[] args) {

        Scanner entrada = new Scanner(System.in);

        System.out.println("Ingrese un numero");
        int n = entrada.nextInt();

        System.out.print("El factorial es: ");

        for (int i = n; i >= 0; i--) {
            System.out.print(i + "x ");
        }
    }
}
