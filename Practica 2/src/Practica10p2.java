
import java.util.Scanner;

/**
 *
 * @author BAAM
 */
public class Practica10p2 {

    public static void main(String[] args) {

        Scanner entrada = new Scanner(System.in);

        System.out.println("Ingrese un numero");
        int n = entrada.nextInt();

        int i = n;

        System.out.println("El factorial es: ");

        while (i >= 0) {
            System.out.print(i + "x ");

            i--;
        }
    }
}
