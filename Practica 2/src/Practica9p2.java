/**
 *
 * @author BAAM
 */
public class Practica9p2 {

    public static void main(String[] args) {
        int acum = 0;
        int i = 0;
        while (i <= 10000) {
            if (i % 2 == 0) {
                System.out.print(i + ",");
            } else {
                acum = acum + i;
            }
            i++;
        }
        System.out.println("");
        System.out.println("La suma de los numeros impares es: " + acum);

    }
}
